import React from "react";
import '../styles/Spinner.css'

function spinner() {
  return (
    <div className="loader"></div>
  );
}

export default spinner;
