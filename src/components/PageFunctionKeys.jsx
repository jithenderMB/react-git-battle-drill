import React, { Component } from "react";

class pageFunctionKeys extends Component {
  render() {
    return (
      <li className="popular-page-options">
        <button
          className={
            this.props.darkTheme
              ? "popular-page-buttons dark-font"
              : "popular-page-buttons"
          }
          onClick={() =>
            this.props.handledisplayUsers(
              this.props.displayCategoryObject.category
            )
          }
        >
          {this.props.displayCategoryObject.category}
        </button>
      </li>
    );
  }
}

export default pageFunctionKeys;
