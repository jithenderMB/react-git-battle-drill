import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import "../styles/Main.css";

import Navbar from "./Navbar";

import BattlePage from "./BattlePage";
import PopularPage from "./PopularPage";
import ResultPage from "./ResultPage";



class main extends Component {
  state = {
    dark: false
  }

  handleTheme = () => {
    const { dark } = this.state;
    if (dark) {
      this.setState({
        dark: false,
      });
    }
    else{
      this.setState({
        dark: true,
      });
    }
  };

  render() {
    return (
      <BrowserRouter>
        <div className={this.state.dark? 'main dark-bg-main' : 'main'}>
          <div className="sub-main">
            <Navbar darkTheme={this.state.dark} themeChanger={this.handleTheme}/>
            <Switch>
              <Route exact path="/" component={(props)=><PopularPage darkTheme={this.state.dark}{...props}/>} />
              <Route exact path="/battle" component={(props)=><BattlePage darkTheme={this.state.dark}{...props}/>} />
              {/* <Route exact path="/battle/:UserName" component={ResultPage} /> */}
              <Route exact path="/battle/:UserName" component={(props)=><ResultPage darkTheme={this.state.dark}{...props}/>} />

            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default main;
