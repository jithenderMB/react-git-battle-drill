import React, { Component } from 'react'
import UserImage from '../images/battle-users-logo.svg'
import PlaneImage from '../images/battle-plane.svg'
import PrizeImage from '../images/battle-prize-logo.svg'


class BattleImageSection extends Component {
  render() {
    return (
      <div>
          <img src={UserImage}/>
          <img src={PlaneImage}/>
          <img src={PrizeImage}/>
      </div>
    )
  }
}

export default BattleImageSection