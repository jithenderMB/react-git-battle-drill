import React, { Component } from "react";
import PageOptions from "./PageOptions";
import UserCards from "./UserCards";
import Spinner from "./Spinner";

const displayUsersArray = [
  {
    category: "All",
  },
  {
    category: "JavaScript",
  },
  {
    category: "Ruby",
  },
  {
    category: "Java",
  },
  {
    category: "CSS",
  },
  {
    category: "Python",
  },
];

class PopularPage extends Component {
  state = {
    users: "",
    displayCategory: "All",
  };

  componentDidMount = () => {
    this.fetchData(this.state.displayCategory);
  };

  fetchData = (display) => {
    fetch(
      `https://api.github.com/search/repositories?q=stars:%3E1+language:${display}&sort=stars&order=desc&type=Repositories`
    )
      .then((res) => res.json())
      .then((res) => {
        this.setState({
          users: res.items,
        });
      });
  };

  handleDisplayUsersCategory = (category) => {
    this.setState(
      {
        displayCategory: category,
      },
      () => this.fetchData(this.state.displayCategory)
    );
  };

  render() {
    // console.log(this.props.darkTheme);
    return (
      <React.Fragment>
        <PageOptions
          UsersArray={displayUsersArray}
          displayUsers={this.handleDisplayUsersCategory}
          darkTheme={this.props.darkTheme}
        />

        <ul className="d-flex flex-wrap justify-content-around">
          {!this.state.users ? (
            <Spinner />
          ) : (
            this.state.users.map((user, index) => (
              <UserCards
                key={user.id}
                data={user}
                count={index}
                darkTheme={this.props.darkTheme}
              />
            ))
          )}
        </ul>
      </React.Fragment>
    );
  }
}

export default PopularPage;
