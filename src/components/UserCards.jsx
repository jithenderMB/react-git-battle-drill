import React, { Component } from "react";
import UserIcon from "../images/user-logo.svg";
import StarIcon from "../images/star-logo.svg";
import GitBranchIcon from "../images/git-branch.svg";
import WarningIcon from "../images/warning-sign.svg";

class userCards extends Component {
  render() {
    console.log(this.props.darkTheme);

    return (
      <div className={this.props.darkTheme? 'popular-user-card-conatiner d-flex flex-column justify-content-between dark-bg-cards' : 'popular-user-card-conatiner d-flex flex-column justify-content-between '}>
        <h4 className={this.props.darkTheme?"popular-user-card-number dark-font" : 'popular-user-card-number'}>#{this.props.count + 1}</h4>
        <img
          className="popular-user-image"
          src={this.props.data.owner.avatar_url}
          alt="user-image.png"
        />
        <h2>
          <a className="popular-user-title" href={this.props.data.html_url} target='_blank'>
            {this.props.data.owner.login}
          </a>
        </h2>

        <ul className="popular-user-card-details-container">
          <li className="popular-user-card-each-detail-container">
            <div className="each-detail-sub-container">
              <img
                className="each-detail-icon"
                src={UserIcon}
                alt="user-icon.svg"
              />

              <a
                className={this.props.darkTheme?"each-detail-content each-detail-title dark-font  dark-font-card" : 'each-detail-content each-detail-title'}
                href={this.props.data.owner.html_url} target='_blank'
              >
                {this.props.data.owner.login}
              </a>
            </div>
          </li>

          <li className="popular-user-card-each-detail-container">
            <div className="each-detail-sub-container">
              <img className="each-detail-icon" src={StarIcon} alt="star-icon.svg" />
              <a className={this.props.darkTheme?"each-detail-content dark-font dark-font-card" : 'each-detail-content'} href="#">
                {this.props.data.stargazers_count} stars
              </a>
            </div>
          </li>

          <li className="popular-user-card-each-detail-container">
            <div className="each-detail-sub-container">
              <img className="each-detail-icon" src={GitBranchIcon} alt="git-branch-icon.svg" />
              <a className={this.props.darkTheme?"each-detail-content dark-font dark-font-card" : 'each-detail-content'} href="#">
                {this.props.data.forks_count} forks
              </a>
            </div>
          </li>

          <li className="popular-user-card-each-detail-container">
            <div className="each-detail-sub-container">
              <img className="each-detail-icon" src={WarningIcon} alt="warning-icon.svg" />
              <a className={this.props.darkTheme?"each-detail-content dark-font dark-font-card" : 'each-detail-content'} href="#">
                {this.props.data.open_issues_count} open issues
              </a>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}

export default userCards;

