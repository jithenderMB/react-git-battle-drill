import React, { Component } from "react";
import BattleImageSection from "./BattleImageSection";

import UserImage from "../images/battle-users-logo.svg";
import PlaneImage from "../images/battle-plane.svg";
import PrizeImage from "../images/battle-prize-logo.svg";
import ClossIcon from "../images/cross-icon.svg";

import { Link } from "react-router-dom";

class BattlePage extends Component {
  state = {
    player1: {
      data: {},
      userInput: "",
      submitButton: true,
      playerCard: false,
    },
    player2: {
      data: {},
      submitButton: true,
      playerCard: false,
    },

  };



  fetchUserData = (name, playerNumber) => {
    fetch(` https://api.github.com/users/${name}`)
      .then((res) => res.json())
      .then((res) => {
        if (playerNumber === "player1") {
          let player = { ...this.state.player1 };
          player.data = { ...res };
          player.userInput = "";
          player.playerCard = true;
          this.setState({
            player1: player,
          });
        } else if (playerNumber === "player2") {
          let player = { ...this.state.player2 };
          player.data = { ...res };
          player.userInput = "";
          player.playerCard = true;
          this.setState({
            player2: player,
          });
        }
      });
  };
  handleSubmitPlayer = (e, player, playerNumber) => {
    e.preventDefault();
    player !== ""
      ? this.fetchUserData(player, playerNumber)
      : alert("Enter Player One Name");
  };
  playerOneInput = (e) => {
    let player = { ...this.state.player1 };
    player.userInput = e.target.value;
    player.submitButton = e.target.value === "" ? true : false;
    this.setState({
      player1: player,
    });
  };
  playerTwoInput = (e) => {
    let player = { ...this.state.player2 };
    player.userInput = e.target.value;
    player.submitButton = e.target.value === "" ? true : false;
    this.setState({
      player2: player,
    });
  };

  handlePlayerCard = (player) => {
    if (player === "player1") {
      let player = { ...this.state.player1 };
      player.data = {};
      player.userInput = "";
      player.playerCard = false;
      this.setState({
        player1: player,
      });
    } else if (player === "player2") {
      let player = { ...this.state.player2 };
      player.data = {};
      player.userInput = "";
      player.playerCard = false;
      this.setState({
        player2: player,
      });
    }
  };

  handleBattleResult = () => {
    if (this.state.player1.data.followers > this.state.player2.data.followers) {
      return {
        Winner: this.state.player1.data,
        Loser: this.state.player2.data,
      };
    } else if (
      this.state.player1.data.followers < this.state.player2.data.followers
    ) {
      return {
        Winner: this.state.player2.data,
        Loser: this.state.player1.data,
      };
    } else {
      return { 
        status: 'tie',
        Winner: this.state.player1.data,
        Loser: this.state.player2.data,
      };
    }
  };

  render() {
    // console.log(this.props.darkTheme);
    return (
      <div className="d-flex flex-column">
        <div className="battle-page-icons-container">
          <h1 className={this.props.darkTheme?"battle-page-icons-title text-center dark-font" :"battle-page-icons-title text-center"}>Instructions</h1>
          <ul className="d-flex justify-content-center text-center p-0">
            <li className="battle-page-icon">
              <h3 className={this.props.darkTheme?"battle-page-icon-tilte dark-font" :"battle-page-icon-tilte"}>Enter two Github users</h3>
              <img className={this.props.darkTheme?'battle-page-icon-image dark-bg-cards' :"battle-page-icon-image"} src={UserImage} />
            </li>

            <li className="battle-page-icon">
              <h3 className={this.props.darkTheme?"battle-page-icon-tilte dark-font" :"battle-page-icon-tilte"}>Battle</h3>
              <img className={this.props.darkTheme?'battle-page-icon-image dark-bg-cards' :"battle-page-icon-image"} src={PlaneImage} />
            </li>

            <li className="battle-page-icon">
              <h3 className={this.props.darkTheme?"battle-page-icon-tilte dark-font" :"battle-page-icon-tilte"}>See the winner</h3>
              <img className={this.props.darkTheme?'battle-page-icon-image dark-bg-cards' :"battle-page-icon-image"} src={PrizeImage} />
            </li>
          </ul>
        </div>

        <div className="text-center battle-page-players-container">
          <h1 className={this.props.darkTheme?'battle-page-players-title dark-font' :"battle-page-players-title"}>Players</h1>
          <div className="d-flex justify-content-between">


            {!this.state.player1.playerCard && (
              <form
                className="battle-page-player-container d-flex flex-column"
                onSubmit={(e) =>
                  this.handleSubmitPlayer(
                    e,
                    this.state.player1.userInput,
                    "player1"
                  )
                }
              >
                <label htmlFor="" className={this.props.darkTheme? "battle-page-player-title dark-font": "battle-page-player-title"}>
                  Player One
                </label>
                <div className="d-flex">
                  <input
                    className={this.props.darkTheme?"form-control battle-page-player-input input-dark": 'form-control battle-page-player-input'}
                    type="text"
                    placeholder="github username"
                    onChange={this.playerOneInput}
                    value={this.state.player1.userInput || ""}
                  />
                  <button
                    className={this.props.darkTheme?"btn btn-dark battle-page-player-submit-button button-dark" : "btn btn-dark battle-page-player-submit-button"}
                    disabled={this.state.player1.submitButton}
                  >
                    SUBMIT
                  </button>
                </div>
              </form>
            )}

            {this.state.player1.playerCard && (
              <div className="battle-player-fetch-container d-flex flex-column">
                <h3 className={this.props.darkTheme?"battle-player-fetch-title align-self-start dark-font":'battle-player-fetch-title align-self-start'}>Player One</h3>
                <div className="d-flex battle-player-fetch-display-container">
                  <div>
                    <img
                      className="battle-player-fetch-display-image"
                      src={this.state.player1.data.avatar_url}
                      alt="player1-image.png"
                    />
                    <a
                      className="battle-player-fetch-display-title"
                      href={this.state.player1.data.html_url}
                      target="_blank"
                    >
                      {this.state.player1.data.login}
                    </a>
                  </div>
                  <button
                    className="battle-player-fetch-display-button"
                    onClick={() => this.handlePlayerCard("player1")}
                  >
                    <img src={ClossIcon} alt="close-card.png" />
                  </button>
                </div>
              </div>
            )}
            {!this.state.player2.playerCard && (
              <form
                className="battle-page-player-container  d-flex flex-column"
                onSubmit={(e) =>
                  this.handleSubmitPlayer(
                    e,
                    this.state.player2.userInput,
                    "player2"
                  )
                }
              >
                <label htmlFor="" className={this.props.darkTheme? "battle-page-player-title dark-font": "battle-page-player-title"}>
                  Player Two
                </label>
                <div className="d-flex">
                  <input
                    className={this.props.darkTheme?"form-control battle-page-player-input input-dark": 'form-control battle-page-player-input'}
                    type="text"
                    placeholder="github username"
                    onChange={this.playerTwoInput}
                    value={this.state.player2.userInput || ""}
                  />
                  <button
                    className={this.props.darkTheme?"btn btn-dark battle-page-player-submit-button button-dark" : "btn btn-dark battle-page-player-submit-button"}
                    disabled={this.state.player2.submitButton}
                  >
                    SUBMIT
                  </button>
                </div>
              </form>
            )}


            {this.state.player2.playerCard && (
              <div className="battle-player-fetch-container d-flex flex-column">
                <h3 className={this.props.darkTheme?"battle-player-fetch-title align-self-start dark-font":'battle-player-fetch-title align-self-start'}>Player One</h3>
                <div className="d-flex battle-player-fetch-display-container">
                  <div>
                    <img
                      className="battle-player-fetch-display-image"
                      src={this.state.player2.data.avatar_url}
                      alt="player2-image.png"
                    />
                    <a
                      className="battle-player-fetch-display-title"
                      href={this.state.player2.data.html_url}
                      target="_blank"
                    >
                      {this.state.player2.data.login}
                    </a>
                  </div>
                  <button
                    className="battle-player-fetch-display-button"
                    onClick={() => this.handlePlayerCard("player2")}
                  >
                    <img src={ClossIcon} alt="close-card.png" />
                  </button>
                </div>
              </div>
            )}
          </div>
          {this.state.player1.playerCard && this.state.player2.playerCard ? (
            <Link
              className={this.props.darkTheme?"battle-page-battle-button btn btn-dark button-battle-dark": 'battle-page-battle-button btn btn-dark'}

              to={{
                pathname: `/battle/results?playerOne=${this.state.player1.data.login}&playerTwo=${this.state.player2.data.login}`,
                playersData: this.handleBattleResult(),
              }}
            >
              BATTLE
            </Link>
          ) : 
          null}
        </div>

  
      </div>
    );
  }
}

export default BattlePage;
