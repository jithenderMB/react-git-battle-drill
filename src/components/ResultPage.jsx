import React, { Component } from "react";

import UserBattleIcon from "../images/battle-result-user-logo.svg";
import CompassBattleIcon from "../images/battle-result-compass.svg";
import UsersBattleIcon from "../images/battle-result-users-logo.svg";
import TwoUserBattleIcon from "../images/battle-result-two-user-logo.svg";
import CodeBattleIcon from "../images/battle-result-code-logo.svg";
import { Link } from "react-router-dom";

export class ResultPage extends Component {
  render() {
    // console.log(this.props.darkTheme);

    return (
      <div className="d-flex flex-column">
        <div className="result-page-players-container d-flex justify-content-around">
          <div className={this.props.darkTheme?"d-flex flex-column result-page-player-container dark-bg-cards" : 'd-flex flex-column result-page-player-container'}>
            <h4 className={this.props.darkTheme?"result-page-player-status align-self-center dark-font":'result-page-player-status align-self-center'}>
              {this.props.location.playersData?.status ? "Tie" : "Winner"}
            </h4>
            <img
              className="result-page-player-image align-self-center"
              src={this.props.location.playersData.Winner.avatar_url}
              alt=""
            />
            <h4 className={this.props.darkTheme?"result-page-player-score align-self-center dark-font":'result-page-player-score align-self-center'}>
              Score: {this.props.location.playersData.Winner.followers}
            </h4>
            <h2 className="align-self-center">
              <a className="result-page-player-login" href="">
                {this.props.location.playersData.Winner.login}
              </a>
            </h2>
            <ul className="result-page-player-details-container">
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={UserBattleIcon} alt="" />
                {this.props.location.playersData.Winner.name}
              </li>
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={CompassBattleIcon} alt="" />
                {this.props.location.playersData.Winner.location}
              </li>
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={UsersBattleIcon} alt="" />
                {this.props.location.playersData.Winner.followers} Followers
              </li>
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={TwoUserBattleIcon} alt="" />
                {this.props.location.playersData.Winner.following} Following
              </li>
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={CodeBattleIcon} alt="" />
                {this.props.location.playersData.Winner.public_repos}{" "}
                repositories
              </li>
            </ul>
          </div>
          <div className={this.props.darkTheme?"d-flex flex-column result-page-player-container dark-bg-cards" : 'd-flex flex-column result-page-player-container'}>
            <h4 className={this.props.darkTheme?"result-page-player-status align-self-center dark-font":'result-page-player-status align-self-center'}>
              {this.props.location.playersData?.status ? "Tie" : "Loser"}
            </h4>
            <img
              className="result-page-player-image align-self-center"
              src={this.props.location.playersData.Loser.avatar_url}
              alt=""
            />
            <h4 className={this.props.darkTheme?"result-page-player-score align-self-center dark-font":'result-page-player-score align-self-center'}>
              Score: {this.props.location.playersData.Loser.followers}
            </h4>
            <h2 className="align-self-center">
              <a className="result-page-player-login" href="">
                {this.props.location.playersData.Loser.login}
              </a>
            </h2>
            <ul className="result-page-player-details-container">
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={UserBattleIcon} alt="" />
                {this.props.location.playersData.Loser.name}
              </li>
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={CompassBattleIcon} alt="" />
                {this.props.location.playersData.Loser.location}
              </li>
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={UsersBattleIcon} alt="" />
                {this.props.location.playersData.Loser.followers} Followers
              </li>
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={TwoUserBattleIcon} alt="" />
                {this.props.location.playersData.Loser.following} Following
              </li>
              <li className={this.props.darkTheme?"result-page-player-details-sub-container dark-font":'result-page-player-details-sub-container'}>
                <img className="result-page-player-details-icons" src={CodeBattleIcon} alt="" />
                {this.props.location.playersData.Loser.public_repos}{" "}
                repositories
              </li>
            </ul>
          </div>
        </div>

        <Link to="/battle" className={this.props.darkTheme?"result-page-reset-button btn btn-dark align-self-center button-battle-dark":'result-page-reset-button btn btn-dark align-self-center'}>
          RESET
        </Link>
      </div>
    );
  }
}

export default ResultPage;
