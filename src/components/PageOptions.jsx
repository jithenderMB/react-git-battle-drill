import React, { Component } from "react";
import PageFunctionKeys from "./PageFunctionKeys";

class pageOptions extends Component {
  render() {
    return (
      <ul className=" pt-2 pb-2 d-flex justify-content-center">
        {this.props.UsersArray.map((categoryType) => {
          return (
            <PageFunctionKeys
              key={categoryType.category}
              displayCategoryObject={categoryType}
              handledisplayUsers={this.props.displayUsers}
              darkTheme={this.props.darkTheme}
            />
          );
        })}
      </ul>
    );
  }
}

export default pageOptions;
