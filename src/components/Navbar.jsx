import React, { Component } from "react";
import { Link } from "react-router-dom";

class navbar extends Component {
  
  render() {
    return (
      <nav className="d-flex justify-content-between align-items-center ">
        <ul className="d-flex nav">
          <li>
            <Link to="/" className={this.props.darkTheme? 'nav-options dark-font' : "nav-options"}>
              Popular
            </Link>
          </li>
          <li>
            <Link to="/battle" className={this.props.darkTheme? 'nav-options nav-battle-option dark-font' : "nav-options nav-battle-option"}>
              Battle
            </Link>
          </li>
        </ul>
        <button className="button btn-clear" onClick={this.props.themeChanger}>
          {this.props.darkTheme ? '💡' : '🔦'}
        </button>
      </nav>
    );
  }
}

export default navbar;
